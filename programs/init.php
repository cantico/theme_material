<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by kanzi ({@link https://wrapbootstrap.com/theme/kanzi-multi-purpose-template-WB08808B5})
 */

require_once 'base.php';


function theme_material_upgrade($version_base,$version_ini) {
	global $babBody, $babInstallPath;
	include_once $babInstallPath."utilit/eventincl.php";
	bab_addEventListener('bab_eventPageRefreshed', 'theme_material_onPageRefreshed', 'addons/theme_material/init.php', 'theme_material');

	return true;
}

function theme_material_onDeleteAddon() {
	global $babInstallPath;
	include_once $babInstallPath."utilit/eventincl.php";
 	bab_removeEventListener('bab_eventPageRefreshed', 'theme_material_onPageRefreshed', 'addons/theme_material/init.php');
 	return true;
}

function theme_material_onPageRefreshed() {
	global $babSkin;
	if ('theme_material' === $babSkin) {
		$jquery = bab_functionality::get('jquery');
		if ($jquery !== false) {
			$jquery->includeCore();
		}
		$icons = bab_functionality::get('Icons');
		if ($icons != false) {
		    $icons->includeCss();
		}
	}
}

