; <?php/*

[general]
name							="theme_material"
version							="1.0"
description						="Skin Material"
description.fr					="Skin Material"
icon							="jaspe-logo.png"
image							="jaspe-mini.png"
delete							="1"
addon_access_control			="0"
ov_version						="8.6"
author							="Cantico"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
php_version						="5.1.0"
addon_type						="THEME"

[functionalities]
jquery="Available"

[addons]
LibFileManagement           =">=0.3.1"
jquery						=">=1.7.1.5"

;*/ ?>