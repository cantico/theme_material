<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd7823fa164bd49238e5f7c36ae94f08c
{
    public static $prefixLengthsPsr4 = array (
        'L' => 
        array (
            'Leafo\\ScssPhp\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Leafo\\ScssPhp\\' => 
        array (
            0 => __DIR__ . '/..' . '/leafo/scssphp/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd7823fa164bd49238e5f7c36ae94f08c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd7823fa164bd49238e5f7c36ae94f08c::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
